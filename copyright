Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: iso2mesh
Upstream-Contact: fangqq@gmail.com
Source: https://github.com/fangq/iso2mesh
Comment:  Iso2Mesh is a MATLAB/Octave-based mesh generation toolbox,
 designed for easy creation of high quality surface and
 tetrahedral meshes from 3D volumetric images. It contains
 a rich set of mesh processing scripts/programs, working
 either independently or interacting with external free
 meshing utilities. Iso2Mesh toolbox can directly convert
 a 3D image stack, including binary, segmented or gray-scale
 images such as MRI or CT scans, into quality volumetric
 meshes. This makes it particularly suitable for multi-modality
 medical imaging data analysis and multi-physics modeling.
 Iso2Mesh is cross-platform and is compatible with both MATLAB
 and GNU Octave.


Files: *
Copyright: 2007-2020 Qianqian Fang
License: GPL-2+

Files: src/cgalsurf/* tools/cgalmesh/* tools/cgalsimp2/*
Copyright: 2009-2020 Qianqian Fang
           2009 The CGAL Project
License: Permissive

Files: src/cork/*
Copyright: 2016 Qianqian Fang
           2013 Gilbert Bernstein
License: LGPL-3+

Files: src/meshfix/*
Copyright: 2010, Marco Attene (IMATI-GE / CNR)
License: GPL-2+

Files: debian/*
Copyright: 2020 Qianqian Fang
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.


License: LGPL-3+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.

License: Permissive
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

