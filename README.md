Commands to build the package, please copy and paste to a terminal

```
VER=1.9.5
PKG=iso2mesh
wget https://github.com/fangq/${PKG}/archive/v${VER}.tar.gz
tar zxvf v${VER}.tar.gz

cd ${PKG}-${VER}/tools/
rm -rf tetgen cork meshfix
wget https://github.com/fangq/cork/archive/v0.9.tar.gz
tar zxvf v0.9.tar.gz
mv cork-0.9 cork
rm -rf v0.9.tar.gz

wget https://github.com/fangq/meshfix/archive/v1.2.2.tar.gz
tar zxvf v1.2.2.tar.gz
mv meshfix-1.2.2 meshfix
rm -rf v1.2.2.tar.gz

cd ../../

# remove upstream binary files
cd ${PKG}-${VER}
rm -rf bin/*.mex* bin/*.dll bin/*.exe
mv tools src
rm src/tetgen
mkdir inst
mv *.m inst
mv *.fig inst
mv ChangeLog.txt NEWS
mv COPYING.txt COPYING
cd ..

# recreate the orig package
tar zcvf octave-${PKG}_${VER}.orig.tar.gz ${PKG}-${VER}
cd ${PKG}-${VER}

#download the debian packaging files
git clone https://salsa.debian.org/fangq/octave-${PKG}.git debian

#build deb package
debuild -us -uc
```
